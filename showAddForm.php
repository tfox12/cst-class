<!-- 
 this is a partial page
 purpose is to show the "Add Devotion" form
 even though the file extension is .php, all of the code is html.
 -->
 
<div class="form-container">
 <h2>Add a devotion</h2>
 <p>Fill out all the fields and submit</p>
 
	<form action="processAddItem.php">
 		Devotion Topic:<input type="text" name="devotionTopic"></input><br>
 		Devotion Title:<textarea rows="5" cols="50" name="devotionTitle"></textarea><br>
 		Devotion Body:<textarea rows="5" cols="50" name="devotionBody"></textarea>
 		<button type="submit">Add</button>
 	</form>
 	
</div>