<!-- 
this is a partial page
purpose is to show a search form under the top menu
even though the file extension is .php, all of the code on this page is html
 -->

<div class="form-container">

<h2>Search for a devotion</h2>
<p>Fill out any of these fields and search</p>
<form action="searchDevotion.php">
Devotion Topic:<input type="text" name="devotionTopic"></input><br>
Devotion Title:<input type="text" name="devotionTitle"></input><br>
Devotion Body:<input type="text" name="devotionBody"></input><br>
<button type="submit">Search</button>
</form>
</div>